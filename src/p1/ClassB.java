package p1;
public class ClassB {
    public static void main(String[] args) throws Exception {
        ClassA classA = new ClassA();
        classA.i = 2; //i is not visible
        classA.j = 3;
        classA.k = 4;
        classA.l = 5;
    }
}
