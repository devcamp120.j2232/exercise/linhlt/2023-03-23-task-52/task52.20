package p2;

import p1.ClassA;

public class ClassC extends ClassA {
    public static void main(String[] args) throws Exception {
        ClassC classA = new ClassC();
        classA.i = 2; //i is not visible
        classA.j = 3; //j is not visible
        classA.k = 4; 
        classA.l = 5;
    }
}
